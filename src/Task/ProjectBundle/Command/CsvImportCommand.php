<?php
namespace Task\ProjectBundle\Command;

use Task\ProjectBundle\Service\CSVImport;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class CsvImportCommand extends Command
{
    
    private $csvImport;
    public function __construct(CSVImport $csvImport)
    {
        parent::__construct();
        $this->csvImport = $csvImport;
    }

    // This method configures the command
    protected function configure()
    {
        //Create a Command csv:import to load the csv file from command line.
        $this
            ->setName('csv:import')
            ->setDescription('Imports the mock CSV data file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        // Set the title for InputInterface
        $io->title('Importing the CSV File');
        try
        {
            // call the CSVImport service
            $this->csvImport->ServiceProviderImport();    
        }
        catch(Exception $e)
        {
            return 'Message: ' .$e->getMessage();
        }
        
        // Print a success message on the console.
        $io->success('CSV Imported Successfully!');
    }
}