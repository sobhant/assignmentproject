<?php
namespace Task\ProjectBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class VouchersAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
         $formMapper->add('status', TextType::class);
         $formMapper->add('expiry_date', DateTimeType::class);
         $formMapper->add('voucher_id', TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
         $datagridMapper->add('status');
         $datagridMapper->add('expiry_date');
         $datagridMapper->add('voucher_id');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('status');
        $listMapper->addIdentifier('expiry_date');
        $listMapper->addIdentifier('voucher_id');
    }
}