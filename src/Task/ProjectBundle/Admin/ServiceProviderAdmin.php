<?php
namespace Task\ProjectBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ServiceProviderAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('serviceProvider_Name', TextType::class);
        $formMapper->add('voucher_limit', IntegerType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('serviceProvider_Name');
        $datagridMapper->add('voucher_limit');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('serviceProvider_Name');
        $listMapper->addIdentifier('voucher_limit');

    }
}