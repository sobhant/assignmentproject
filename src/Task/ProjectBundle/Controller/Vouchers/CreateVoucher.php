<?php
namespace Task\ProjectBundle\Controller\Vouchers;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class CreateVoucher extends FOSRestController 
{
	 /**
	 * @Rest\Post("/vouchers")
	 * Method to Create new Voucher.
	 */

	public function CreateVouchers(Request $request)
	{
	 	try
	 	{
	 		// Get the parameter from the request.
		   	$expiry_date = $request->get('expiry_date');

		   	$user = $request->attributes->get('user_object');

		   	// Check whether the parameters are not empty.
		   	// If they are empty then return an error message.
		 	if(empty($expiry_date))
		 	{
		   		return array("Error" => "Fields Cannot be Empty");
		 	}   

		 	// Get the validate Service
			$validate = $this->get('validate');
			
			// Check whether the user has created the service provider or not.
			$ServiceProvider = $validate->validateServiceProvider($user->getId());

			// Call the PostVoucher method inside the validate service to create the new voucher
			return $validate->PostVoucher($user, $ServiceProvider, $expiry_date);	
	 	}
	   	
	   	catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
	}
}