<?php
namespace Task\ProjectBundle\Controller\Vouchers;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class RedeemVoucher extends FOSRestController 
{
   /**
 	* @Rest\Put("/redeem/{voucherID}")
 	* Method to redeem voucher.
 	*/

 	public function redeemVouchers($voucherID,Request $request)
 	{
 		try
 		{
	 		$data = new Vouchers();
		    $em = $this->getDoctrine()->getManager();

		    // Call the validate service
			$validate = $this->get('validate');

			$user = $request->attributes->get('user_object');

			// Check whether the input voucher id is correct or not.
			$findVoucher = $validate->findVoucher($voucherID, $user);
			if($findVoucher == null)
			{
				return array("Error" => "No Such Voucher Present");
			}
			
			// Call the RedeemVoucher method inside the validate service to Redeem the new voucher
			return $validate->RedeemVoucher($findVoucher);
			
 		}

 		catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
	}
}