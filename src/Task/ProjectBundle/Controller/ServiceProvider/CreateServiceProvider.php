<?php
namespace Task\ProjectBundle\Controller\ServiceProvider;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;



class CreateServiceProvider extends FOSRestController 
{
	/**
	 * @Rest\Post("/serviceprovider")
	 * Method to Create new Service Provider.
	 */

	 public function PostServiceProvider(Request $request)
	 {
	 	try
	 	{
	 		// Collect the parameters from the request.
		 	$name = $request->get('name');
		   	$limit = $request->get('limit');

		   	$user = $request->attributes->get('user_object');

		   	// Check whether the parameters are not empty.
		   	// If they are empty then return an error message.
		 	if(empty($name) || empty($limit))
		 	{
				return array("Error" =>"Fields Cannot be Empty");
		 	} 
		    
			return $this->get('validate')->PostServiceProvider($user,$name,$limit);	
	 	}
	   	
	   	catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
	 }
}