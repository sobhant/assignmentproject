<?php
namespace Task\ProjectBundle\Service;

use Task\ProjectBundle\Entity\TransactionLog;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\AbstractProcessingHandler;

class MonologDBHandler extends AbstractProcessingHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * MonologDBHandler constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    /**
     * Called when writing to our database
     * @param array $record
     */
    protected function write(array $record)
    {
        try
        {
            $logEntry = new TransactionLog();

            // Call the setters method of the TransactionLog Entity.
            $logEntry->setMessage($record['message']);
            $logEntry->setLevel($record['level']);
            $logEntry->setLevelName($record['level_name']);
            $logEntry->setExtra($record['extra']);
            $logEntry->setContext($record['context']);
            
            $this->em->persist($logEntry);
            $this->em->flush();    
        }
        catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
        
    }
}