<?php
namespace Task\ProjectBundle\Service;
use Task\ProjectBundle\Entity\User;
use Task\ProjectBundle\Entity\ServiceProvider;
use Task\ProjectBundle\Entity\Vouchers;
use Doctrine\ORM\EntityManager;


class Validate
{
	/**
     *
     * @var EntityManager 
     */
    protected $em;
	public function __construct(EntityManager $entityManager)
	{
	    $this->em = $entityManager;
	}


	/*
		Function to check basic authorization. This function returns null if input credentials
		mismatch, else it return the user object.
	*/

	public function validateUserCredentials($username)
	{
		try 
		{
			$em = $this->em;

			// Check whether the username is present or not.
			// If the username is not present then return null.
			// Else return the user object.
		    $user = $em->getRepository('ProjectBundle:User')->findOneBy(array('username' => $username));

		    if(!empty($user))
			{
				return $user;
			}
			
			return null;
		} 

		catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
		
	}

	/*
		Function to validate the api Token set inside the header.
	*/

	public function validateToken($api_token)
	{
		try
		{
			$em = $this->em;

			// Check whether the token is present in the database or not.

		    $user = $em->getRepository('ProjectBundle:User')->findOneBy(array('api_token' => $api_token));

		    if(!empty($user))
			{
				return $user;
			}

			return null;
		}

		catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
	}

	/*
		Function to check whether the user has created the Service Provider or not.
	*/
	public function validateServiceProvider($user_id)
	{
		try
		{
			$em = $this->em;

			// Check whether the user has created the service provider or not. 
			// If not then it returns an error message.
		    $ServiceProvider = $em->getRepository('ProjectBundle:ServiceProvider')->findOneBy(array('created_by' => $user_id));

		    if(!empty($ServiceProvider))
			{
				return $ServiceProvider;
			}
			return null;	
		}

		catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }		
	}

	/*
		Function to check whether the voucher is present or not.
	*/
	public function findVoucher($voucher_id, $user)
	{
		try
		{
			$em = $this->em;

			// Search whether the voucher is present or not. 
			// If the Voucher is not present then return an error message.
		    $vouchers = $em->getRepository('ProjectBundle:Vouchers')->findOneBy(array('voucher_id' => $voucher_id, 'user' => $user));

		    if(!empty($vouchers))
			{
				return $vouchers;
			}

			return null;
		}

		catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
	}

	/*
		Function to create new Service provider.
	*/
	public function PostServiceProvider($user,$name,$limit)
	{
		try
		{
			$em = $this->em;
			$data = new ServiceProvider();

			// call the setters method of service provider
			$data->setServiceProviderName($name);
			$data->setVoucherLimit($limit);
			$data->setServiceProviderID(bin2hex(openssl_random_pseudo_bytes(5)));
			$data->setCreatedBy($user);

			$em->persist($data);		  	 	
			$em->flush();
			return array("Status" => "Success: Service Provider Added");	
		}

		catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }		
	}

	/*
		Function to create new Voucher.
	*/
	public function PostVoucher($user, $ServiceProvider, $expiry_date)
	{
		try
		{
			$em = $this->em;
			$data = new Vouchers();
			
			// Call the setters method of Vouchers Entity.
			$data->setStatus("active");
			$data->setExpiryDate(new \DateTime($expiry_date));				
			$data->setVoucherId(bin2hex(openssl_random_pseudo_bytes(5)));
			$data->setServiceProviderID($ServiceProvider);
			$data->setUser($user);

			// Persist the information
			$em->persist($data);	

			$em->flush();
			return array("Status" => "Success: Voucher Added Successfully");
		}

		catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
	}

	/*
		Function to Redeem vouchers.
	*/
	public function RedeemVoucher($voucher)
	{
		try
		{
			$em = $this->em;

			/*
				Get the status of the voucher. 
				Deny the user to redeem the voucher if the voucher
				status is not active. Also the user cannot re-redeem the 
				voucher if it is already redeemed.
			*/
			$status = $voucher->getStatus();
			if($status == 'redeem')
			{
				return array("Error" => "Voucher Cannot be Re-Used");
			}
			if($status == 'expired')
			{
				return array("Error" => "Voucher Expired");
			}
				
			// Set the voucher status to redeem.
			$voucher->setStatus('redeem');
			$em->flush();
			return array("Status" => "Success: Voucher Redeemed");
		}
		catch(Exception $e)
        {
            throw new Exception($e->getMessage());
        }
	}

}