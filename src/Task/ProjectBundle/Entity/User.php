<?php
namespace Task\ProjectBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{
	/**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;


    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @ORM\Column(type="datetime")
     */
    protected $updated_at;

    /**
     * @ORM\ManyToOne(
     * targetEntity="ServiceProvider",
     * inversedBy="id"
     * )
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $sp;


    /**
     * @var string $api_token
     * 
     * @ORM\Column(type="string")
     */
    protected $api_token;


    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }


    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * Set spId
     *
     * @param integer $spId
     *
     * @return User
     */
    public function setSpId($spId)
    {
        $this->sp_id = $spId;

        return $this;
    }

    /**
     * Get spId
     *
     * @return integer
     */
    public function getSpId()
    {
        return $this->sp_id;
    }
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set sp
     *
     * @param ServiceProvider $sp
     *
     * @return User
     */
    public function setSp(ServiceProvider $sp = null)
    {
        $this->sp = $sp;

        return $this;
    }

    /**
     * Get sp
     *
     * @return ServiceProvider
     */
    public function getSp()
    {
        return $this->sp;
    }

    /**
     * Set apiToken
     *
     * @param string $apiToken
     *
     */
    public function setApiToken($apiToken)
    {
        $this->api_token = $apiToken;

        //return $this;
    }

    /**
     * Get apiToken
     *
     * @return string
     */
    public function getApiToken()
    {
        return $this->api_token;
    }
}
