<?php
namespace Task\ProjectBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`vouchers`")
 * @ORM\HasLifecycleCallbacks
 */
class Vouchers
{
	/**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $id;

    /**
     * @var string $status
     * @ORM\Column(type="string")
     */
    protected $status;

    /**
     * @var datetime $expiry_date
     *
     * @ORM\Column(type="datetime")
     */
    protected $expiry_date;

    /**
     * @var string $voucher_id
     * @ORM\Column(type="string")
     */
    protected $voucher_id;


    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @ORM\Column(type="datetime")
     */
    protected $updated_at;


    /**
     * @var string $serviceProvider_ID
     * @ORM\ManyToOne(
     * targetEntity="ServiceProvider", inversedBy="serviceProvider_ID" )
     * @ORM\JoinColumn(name="serviceProvider_ID", referencedColumnName="id")
     */
    protected $serviceProvider_ID;

    /**
     * @ORM\ManyToOne(
     * targetEntity="User", inversedBy="id" )
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;



    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Vouchers
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set expiryDate
     *
     * @param \DateTime $expiryDate
     *
     * @return Vouchers
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiry_date = $expiryDate;

        return $this;
    }

    /**
     * Get expiryDate
     *
     * @return \DateTime
     */
    public function getExpiryDate()
    {
        return $this->expiry_date;
    }

    /**
     * Set voucherId
     *
     * @param string $voucherId
     *
     * @return Vouchers
     */
    public function setVoucherId($voucherId)
    {
        $this->voucher_id = $voucherId;

        return $this;
    }

    /**
     * Get voucherId
     *
     * @return string
     */
    public function getVoucherId()
    {
        return $this->voucher_id;
    }


    /**
     * Set user
     *
     * @param User $user
     *
     * @return Vouchers
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set serviceProviderID
     *
     * @param \Task\ProjectBundle\Entity\ServiceProvider $serviceProviderID
     *
     * @return Vouchers
     */
    public function setServiceProviderID(\Task\ProjectBundle\Entity\ServiceProvider $serviceProviderID = null)
    {
        $this->serviceProvider_ID = $serviceProviderID;

        return $this;
    }

    /**
     * Get serviceProviderID
     *
     * @return \Task\ProjectBundle\Entity\ServiceProvider
     */
    public function getServiceProviderID()
    {
        return $this->serviceProvider_ID;
    }
}
