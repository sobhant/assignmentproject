<?php

namespace Task\ProjectBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`transaction_log`")
 * @ORM\HasLifecycleCallbacks
 */
class TransactionLog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string $message
     * @ORM\Column(type="string")
     */
    private $message;

    /**
     * @var array $context
     * @ORM\Column(type="array")
     */
    private $context;

     /**
     * @var integer $level
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @var string $levelName
     * @ORM\Column(type="string")
     */
    private $levelName;

    /**
     * @var array $extra
     * @ORM\Column(type="array")
     */
    private $extra;

    /**
     * @var datetime $created_at
     * @ORM\Column(type="datetime")
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return TransactionLog
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }


    /**
     * Set level
     *
     * @param integer $level
     *
     * @return TransactionLog
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set levelName
     *
     * @param string $levelName
     *
     * @return TransactionLog
     */
    public function setLevelName($levelName)
    {
        $this->levelName = $levelName;

        return $this;
    }

    /**
     * Get levelName
     *
     * @return string
     */
    public function getLevelName()
    {
        return $this->levelName;
    }

    /**
     * Set extra
     *
     * @param array $extra
     *
     * @return TransactionLog
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return array
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TransactionLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }



    /**
     * Set context
     *
     * @param array $context
     *
     * @return TransactionLog
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get context
     *
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     *
     * @ORM\PrePersist
     */
    public function updatedTimestamps()
    {
        $this->setCreatedAt(new \DateTime('now'));
    }
}
