<?php
namespace Task\ProjectBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`service_provider`")
 * @ORM\HasLifecycleCallbacks
 */
class ServiceProvider
{
	/**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\OneToMany(targetEntity="User", mappedBy="sp")
     */
    protected $id;

    /**
     * @var string $serviceProvider_Name
     * @ORM\Column(type="string")
     */
    protected $serviceProvider_Name;

    /**
     * @var string $serviceProvider_ID
     * @ORM\Column(type="string")
     */
    protected $serviceProvider_ID;

    /**
     * @var integer $voucher_limit
     *
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    protected $voucher_limit;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     * 
     * @ORM\Column(type="datetime")
     */
    protected $updated_at;

    /**
     * @ORM\ManyToOne(
     * targetEntity="User",inversedBy="id" )
     * @ORM\JoinColumn(name="createdBy_id", referencedColumnName="id")
     */
    protected $created_by;
 

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }


    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }


    /**
     * Set serviceProviderName
     *
     * @param string $serviceProviderName
     *
     * @return ServiceProvider
     */
    public function setServiceProviderName($serviceProviderName)
    {
        $this->serviceProvider_Name = $serviceProviderName;

        return $this;
    }

    /**
     * Get serviceProviderName
     *
     * @return string
     */
    public function getServiceProviderName()
    {
        return $this->serviceProvider_Name;
    }

    /**
     * Set voucherLimit
     *
     * @param integer $voucherLimit
     *
     * @return ServiceProvider
     */
    public function setVoucherLimit($voucherLimit)
    {
        $this->voucher_limit = $voucherLimit;

        return $this;
    }

    /**
     * Get voucherLimit
     *
     * @return integer
     */
    public function getVoucherLimit()
    {
        return $this->voucher_limit;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return ServiceProvider
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->created_by = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Set serviceProviderID
     *
     * @param string $serviceProviderID
     *
     * @return ServiceProvider
     */
    public function setServiceProviderID($serviceProviderID)
    {
        $this->serviceProvider_ID = $serviceProviderID;

        return $this;
    }

    /**
     * Get serviceProviderID
     *
     * @return string
     */
    public function getServiceProviderID()
    {
        return $this->serviceProvider_ID;
    }

}
