<?php
namespace Task\ProjectBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;


class ResponseListener
{
	private $container;
	
	public function __construct(Container $container) {
		$this->container = $container;
	}

    public function onKernelResponse(FilterResponseEvent $event)
    {
    	$response = $event->getResponse();
        $content = $response->getContent();

        /*
            Call monolog.logger.db Service and write the logs to the database
        */
        $this->container->get('monolog.logger.db')->info('Log', array("Log Message" => $content));
    }    
}