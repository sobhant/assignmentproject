<?php
namespace Task\ProjectBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Task\ProjectBundle\Service\Validate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class RequestListener
{
    protected $validate;
    protected $container;
    public function __construct(Validate $validate, Container $container)
    {
        $this->validate = $validate;
        $this->container = $container;
    }
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $route = $request->attributes->get('_route');
        
        // An Array to define set of routes, so that the listener 
        // will only allow these routes which are present inside the array

        $routed_array = array("task_project_user_createtoken_generatetoken",
            "task_project_serviceprovider_createserviceprovider_postserviceprovider",
            "task_project_vouchers_createvoucher_createvouchers",
            "task_project_vouchers_redeemvoucher_redeemvouchers");

        // Response Object
        $response = new Response();
        $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

        if(in_array($route, $routed_array))
        {
            if($route == 'task_project_user_createtoken_generatetoken')
            {
                // If the request is GetToken, Then validate the Basic Auth Header.
                $username = $request->server->get('PHP_AUTH_USER');
                $password = $request->server->get('PHP_AUTH_PW');

                if (empty($username) || empty($password))
                {
                    $response ->setContent(json_encode(array("Error" => "Headers cannot be blank")));
                    $event->setResponse($response);
                    return;
                }
               
                //Call the service to check whether the header input is correct or not
                $user = $this->validate->validateUserCredentials($username);

                $encoderService = $this->container->get('security.password_encoder');
                $match = $encoderService->isPasswordValid($user, $password);
            
                // If the credential is wrong
                if(!$match)
                {
                    $response ->setContent(json_encode(array("Error" => "Invalid Header Information")));
                    $event->setResponse($response);
                    return;
                }

                // Set the user object in user_object key.
                $request->attributes->set('user_object', $user);
            } 

            else
            {
                $api_token = $request->headers->get('api_token');
                
                // Call the validate service and verify for api_token header.
                $user = $this->validate->validateToken($api_token);

                if(!$user)
                {
                    $response ->setContent(json_encode(array("Error" => "Invalid Token")));
                    $event->setResponse($response);
                    return;
                }
                
                // Set the user object in user_object key.
                $request->attributes->set('user_object', $user);
            }
        }
    }
    
}